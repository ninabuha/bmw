﻿using HtmlAgilityPack;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace webscraper
{
    class Program
    {
        static void Main(string[] args)
        {
            getHtmlInfo();
            Console.ReadKey();


        }

        private static async void getHtmlInfo()
        {
            var url = "https://www.polovniautomobili.com";

            var httpClient = new HttpClient();
            var html = await httpClient.GetStringAsync(url);



            //parsing data

            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(html);

            var products = htmlDocument.DocumentNode.Descendants("select")
                .Where(node => node.GetAttributeValue("id", "")
                .Equals("brand")).ToList();

            var productsItems = products[0].Descendants("option").ToList();


            string inserts = "";

            for (var i=1; i<productsItems.Count; i++)
            {
                Console.WriteLine("Brand: " + productsItems[i].InnerText);

                string brand = productsItems[i].InnerText;
                inserts += "INSERT INTO proizvodjac VALUES (0,'" + brand + "');";


                /*Console.WriteLine("URL: " + product.Descendants("h3")
                .Where(node => node.GetAttributeValue("class", "")
                .Equals("lvtitle")).First().Descendants("a").First()
                .GetAttributeValue("href", ""));

                Console.WriteLine("Price: " +
                    Regex.Match(product.Descendants("li")
                                .Where(node => node.GetAttributeValue("class", "")
                                .Equals("lvprice prc")).First()
                                .Descendants("span").First().InnerText
                                .Trim('\t', '\r', '\n'), @"\d+.\d+"));*/

                Console.WriteLine();

            }

            //runQuery(inserts);




            url += "/auto-oglasi/zastava";

            httpClient = new HttpClient();
            html = await httpClient.GetStringAsync(url);



            //parsing data

            htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(html);

            var models_alfa_romeo = htmlDocument.DocumentNode.Descendants("select")
                .Where(node => node.GetAttributeValue("id", "")
                .Equals("model")).ToList();

            var models_items_alfa_romeo = models_alfa_romeo[0].Descendants("option").ToList();


            inserts = "";

            foreach(var model in models_items_alfa_romeo)
            {

                
                inserts += "INSERT INTO model VALUES (0,'ostalo',81);";


                //Console.WriteLine();

            }

            runQuery(inserts);

            Console.ReadKey();
        }



        private static string runQuery(string text)
        {

            if (text == "")
            {
                return "";
            }

            string MySqlConnectionString = "datasource=localhost;port=3306;username=root;password=root;database=bobanDB";

            MySqlConnection databaseConnection = new MySqlConnection(MySqlConnectionString);
            MySqlCommand command = new MySqlCommand(text, databaseConnection);
            command.CommandTimeout = 60;

            try
            {
                databaseConnection.Open();

                if (text.Split(' ')[0].ToLower().Equals("select"))
                {
                    MySqlDataReader myReader = command.ExecuteReader();


                    if (myReader.HasRows)
                    {

                        StringBuilder builder = new StringBuilder();
                        while (myReader.Read())
                        {

                            for (int i = 0; i < myReader.FieldCount; i++)
                            {
                                if (!myReader.IsDBNull(i))
                                {
                                    builder.Append(myReader.GetString(i));
                                }
                                else
                                {
                                    builder.Append("NULL");
                                }

                                if (i != myReader.FieldCount - 1)
                                {
                                    builder.Append("-");
                                }
                            }

                            builder.AppendLine();
                        }


                        return builder.ToString();
                    }
                    else
                    {
                        return "Query doesn't have result!";
                    }
                }
                else
                {
                    int numberOfRowsAffected = command.ExecuteNonQuery();
                    return numberOfRowsAffected + "has been affected!";
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return "error";
            }
            finally
            {
                databaseConnection.Close();
            }

        }
    }
}
