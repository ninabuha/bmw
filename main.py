import flask
from flask import Flask
from flask import request

from flaskext.mysql import MySQL
from pymysql.cursors import DictCursor

import datetime

from utils import mysql_db, secured

from werkzeug.security import generate_password_hash,check_password_hash




mysql_db = MySQL(cursorclass=DictCursor)

app = Flask(__name__, static_url_path="")
app.secret_key = "sta god"

app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'root'
app.config['MYSQL_DATABASE_DB'] = 'bobanDB'

mysql_db.init_app(app)


@app.route("/")
def index():
    return app.send_static_file("index.html")





# ---------------------LOGIN/LOGOUT-----------------------------

@app.route("/login", methods=["POST"])
def login():
    cr = mysql_db.get_db().cursor()
    data = dict(request.json)
    serviser = dobavi_lozinku(data["korisnicko_ime"])
    lozinka_servisera = serviser["lozinka"]
    uneta_lozinka = data["lozinka"]
    if check_password_hash(lozinka_servisera, uneta_lozinka):
        cr.execute("SELECT * FROM serviser WHERE korisnicko_ime=%(korisnicko_ime)s", data)
        korisnik = cr.fetchone()
        if korisnik is not None:
            flask.session["korisnik"] = korisnik
            return "", 200

        return "", 401
    else:
        return "", 401

def dobavi_lozinku(korisnicko_ime):
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM serviser WHERE korisnicko_ime=%s", (korisnicko_ime, ))
    serviser = cr.fetchone()
    if serviser:
        return serviser
    else:
        return "", 401

@app.route("/logout", methods=["GET"])
def logout():
    flask.session.pop("korisnik", None)
    return "", 200

# -------------------------------------------------------------




# ------------------------RADNI NALOZI-------------------------

@app.route("/radniNalozi", methods=["GET"])
@secured(roles=[1,2])
def dobavljanje_radnih_naloga():
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * from radninalog, automobil, proizvodjac, model,  klijent WHERE radninalog.Automobil_id=automobil.id and radninalog.Klijent_id=klijent.id and automobil.Model_id=model.id and model.Proizvodjac_id=proizvodjac.id order by datum_prijema DESC")
    radni_nalozi = cr.fetchall()
    for a in radni_nalozi:
        if(a["datum_otpusta"]):
            a["datum_prijema"] = a["datum_prijema"].isoformat()
            a["datum_otpusta"] = a["datum_otpusta"].isoformat()
        else:
            a["datum_prijema"] = a["datum_prijema"].isoformat()
    return flask.json.jsonify(radni_nalozi)

@app.route("/automobili_radninalozi", methods=["GET"])
#@secured(roles=[1,2])
def dobavljanje_automobila_radninalozi():
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM automobil, model, proizvodjac where automobil.Model_id=model.id and model.Proizvodjac_id=proizvodjac.id GROUP BY automobil.id order by naziv_proizvodjaca ASC")
    automobili = cr.fetchall()
    return flask.json.jsonify(automobili)

@app.route("/radniNalozi", methods=["POST"])
#@secured(roles=[1,2])
def dodavanje_radnog_naloga():
    db = mysql_db.get_db()
    cr = db.cursor()
    data = dict(request.json)
    datum_prijema = datetime.datetime.now()
    data["datum_prijema"] = datum_prijema
    cr.execute("INSERT INTO radninalog (datum_prijema, problem, automobil_id, klijent_id, resen_problem, status) VALUES(%(datum_prijema)s, %(problem)s, %(automobil_id)s, %(klijent_id)s, %(resen_problem)s, %(status)s)", data)
    db.commit()
    return "", 201

@app.route("/radniNalozi/<int:id>", methods=["GET"])
#@secured(roles=[1,2])
def dobavljanje_radnog_naloga(id):
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * from radninalog, proizvodjac, model, automobil, klijent WHERE model.Proizvodjac_id=proizvodjac.id and automobil.Model_id=model.id and radninalog.Klijent_id=klijent.id and radninalog.Automobil_id=automobil.id and radninalog.id=%s", (id, ))
    radni_nalog = cr.fetchone()
    if(radni_nalog["datum_otpusta"]):
        radni_nalog["datum_prijema"] = radni_nalog["datum_prijema"].isoformat()
        radni_nalog["datum_otpusta"] = radni_nalog["datum_otpusta"].isoformat()
    else:
        radni_nalog["datum_prijema"] = radni_nalog["datum_prijema"].isoformat()
    return flask.json.jsonify(radni_nalog)

@app.route("/radniNalozi/<int:id>", methods=["PUT"])
#@secured(roles=[1])
def izmena_radnog_naloga(id):
    db = mysql_db.get_db()
    cr = db.cursor()
    data = dict(request.json)
    data["id"] = id
    cr.execute("UPDATE radninalog SET problem=%(problem)s, opis_intervencija=%(opis_intervencija)s, utrosen_materijal=%(utrosen_materijal)s, nabavna_cena_materijala=%(nabavna_cena_materijala)s, cena_materijala=%(cena_materijala)s, resen_problem=%(resen_problem)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200

@app.route("/radniNalozi/zatvaranje/<int:id>", methods=["PUT"])
#@secured(roles=[1])
def zatvaranje_radnog_naloga(id):
    db = mysql_db.get_db()
    cr = db.cursor()
    data = dict(request.json)
    data["id"] = id
    data["datum_otpusta"] = datetime.datetime.now()
    data["status"] = "zatvoren"
    cr.execute("UPDATE radninalog SET datum_otpusta=%(datum_otpusta)s, ukupna_cena=%(ukupna_cena)s, status=%(status)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200

@app.route("/radniNalozi/otvaranje/<int:id>", methods=["PUT"])
#@secured(roles=[1])
def otvaranje_radnog_naloga(id):
    db = mysql_db.get_db()
    cr = db.cursor()
    data = dict(request.json)
    data["id"] = id
    data["datum_otpusta"] = None
    data["status"] = "otvoren"
    data["ukupna_cena"] = 0
    cr.execute("UPDATE radninalog SET datum_otpusta=%(datum_otpusta)s, ukupna_cena=%(ukupna_cena)s, status=%(status)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200

@app.route("/radniNalozi/aktivan/<int:id>", methods=["PUT"])
#@secured(roles=[1])
def aktiviraj_radni_nalog(id):
    db = mysql_db.get_db()
    cr = db.cursor()
    data = dict(request.json)
    data["id"] = id
    data["status"] = "aktivan"
    cr.execute("UPDATE radninalog SET status=%(status)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200

@app.route("/radniNalozi/razduzen/<int:id>", methods=["PUT"])
#@secured(roles=[1])
def razduzi_radni_nalog(id):
    db = mysql_db.get_db()
    cr = db.cursor()
    data = dict(request.json)
    data["id"] = id
    data["status"] = "razduzen"
    cr.execute("UPDATE radninalog SET status=%(status)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200

# -------------------------------------------------------------




# ------------------------IZVRSENE AKCIJE-----------------------


@app.route("/izvrseneAkcije", methods=["GET"])
#@secured(roles=[1,2])
def dobavljanje_izvrsenih_akcija():
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM izvrsenaakcija")
    izvrsene_akcije = cr.fetchall()
    return flask.json.jsonify(izvrsene_akcije)

@app.route("/izvrseneAkcije/<int:id>", methods=["GET"])
#@secured(roles=[1,2])
def dobavljanje_izvrsenih_akcija_za_radni_nalog(id):
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM izvrsenaakcija, akcija, serviser where izvrsenaakcija.Akcija_id=akcija.id and izvrsenaakcija.Serviser_id=serviser.id and RadniNalog_id=%s", (id, ))
    izvrsene_akcije = cr.fetchall()
    for a in izvrsene_akcije:
        if(a["datum_izvrsavanja"]):
            a["datum_izvrsavanja"] = a["datum_izvrsavanja"].isoformat()
    return flask.json.jsonify(izvrsene_akcije)

@app.route("/izvrseneAkcije", methods=["POST"])
#@secured(roles=[1,2])
def dodavanje_izvrsene_akcije():
    db = mysql_db.get_db()
    cr = db.cursor()
    data = dict(request.json)
    data["datum_izvrsavanja"] = datetime.datetime.now()
    cr.execute("INSERT INTO izvrsenaakcija (cena_izvrsene_akcije, serviser_id, akcija_id, radniNalog_id, uspesna, datum_izvrsavanja, opis_akcije) VALUES(%(cena_izvrsene_akcije)s, %(serviser_id)s, %(akcija_id)s, %(radniNalog_id)s, %(uspesna)s, %(datum_izvrsavanja)s, %(opis_akcije)s)", data)
    db.commit()
    return "", 201

@app.route("/akcija/<int:id>", methods=["DELETE"])
#@secured(roles=[1])
def brisanje_izvrsene_akcije(id):
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("DELETE FROM izvrsenaakcija WHERE id=%s", (id, ))
    db.commit()
    return "", 204


# -------------------------------------------------------------



# ------------------------KLIJENTI-------------------------

@app.route("/klijenti", methods=["GET"])
#@secured(roles=[1,2])
def dobavljanje_klijenata():
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * from klijent order by ime ASC")
    klijenti = cr.fetchall()
    return flask.json.jsonify(klijenti)

@app.route("/klijenti", methods=["POST"])
#@secured(roles=[1,2])
def dodavanje_klijenta():
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("INSERT INTO klijent (ime, prezime, telefon) VALUES(%(ime)s, %(prezime)s, %(telefon)s)", request.json)
    db.commit()
    return "", 201

@app.route("/klijenti/<int:id>", methods=["GET"])
#@secured(roles=[1,2])
def dobavljanje_jednog_klijenta(id):
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM klijent WHERE id=%s", (id, ))
    klijent = cr.fetchone()
    return flask.jsonify(klijent)

@app.route("/klijenti/<int:id>", methods=["PUT"])
#@secured(roles=[1])
def izmena_klijenta(id):
    db = mysql_db.get_db()
    cr = db.cursor()
    data = dict(request.json)
    data["id"] = id
    cr.execute("UPDATE klijent SET ime=%(ime)s, prezime=%(prezime)s, telefon=%(telefon)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200

@app.route("/ukloniKlijenta/<int:id>", methods=["PUT"])
#@secured(roles=[1])
def uklanjanje_fizickog_lica(id):
    db = mysql_db.get_db()
    cr = db.cursor()
    data = dict(request.json)
    data["obrisano"] = 1
    cr.execute("UPDATE lica SET obrisano=%(obrisano)s WHERE id=%(id)s", data)
    db.commit()
    return "", 204

# -------------------------------------------------------------




#---------------------------AUTOMOBILI-------------------------

@app.route("/proizvodjaci", methods=["GET"])
def dobavljanje_proizvodjaca():
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM proizvodjac")
    proizvodjaci = cr.fetchall()
    return flask.json.jsonify(proizvodjaci)

@app.route("/proizvodjaci", methods=["POST"])
#@secured(roles=[1,2])
def dodavanje_proizvodjaca():
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("INSERT INTO proizvodjac (naziv_proizvodjaca) VALUES(%(naziv_proizvodjaca)s)", request.json)
    db.commit()
    return "", 201

@app.route("/modeli/<int:id>", methods=["GET"])
def dobavljanje_modela(id):
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM model WHERE model.Proizvodjac_id=%s", (id, ))
    modeli = cr.fetchall()
    return flask.json.jsonify(modeli)

@app.route("/modeli", methods=["POST"])
#@secured(roles=[1,2])
def dodavanje_modela():
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("INSERT INTO model (proizvodjac_id, naziv_modela) VALUES(%(proizvodjac_id)s, %(naziv_modela)s)", request.json)
    db.commit()
    return "", 201

@app.route("/automobili", methods=["GET"])
#@secured(roles=[1,2])
def dobavljanje_automobila():
    cr = mysql_db.get_db().cursor() ## trebaće ovde LEFT JOIN izgleda
    cr.execute("SELECT *, GROUP_CONCAT(DISTINCT akcija.naziv_akcije SEPARATOR ', ') as sve_akcije, GROUP_CONCAT(IF(radninalog.resen_problem=0, radninalog.problem, NULL) SEPARATOR ', ') as nereseni_problemi FROM radninalog left join izvrsenaakcija on izvrsenaakcija.RadniNalog_id=radninalog.id left join akcija on izvrsenaakcija.Akcija_id=akcija.id left join automobil on radninalog.Automobil_id=automobil.id left JOIN model on automobil.Model_id=model.id left join proizvodjac on model.Proizvodjac_id=proizvodjac.id left join klijent on radninalog.Klijent_id=klijent.id left join serviser on izvrsenaakcija.Serviser_id=serviser.id GROUP BY automobil.id ORDER BY radninalog.datum_prijema DESC")
    automobili = cr.fetchall()
    return flask.json.jsonify(automobili)

@app.route("/automobili", methods=["POST"])
#@secured(roles=[1,2])
def dodavanje_automobila():
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("INSERT INTO automobil (model_id, broj_sasije) VALUES(%(model_id)s, %(broj_sasije)s)", request.json)
    db.commit()
    return "", 201

@app.route("/automobili/<int:id>", methods=["GET"])
#@secured(roles=[1,2])
def dobavljanje_jednog_automobila(id):
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT *, GROUP_CONCAT(DISTINCT akcija.naziv_akcije SEPARATOR ', ') as sve_akcije, GROUP_CONCAT(IF(radninalog.resen_problem=0, radninalog.problem, NULL) SEPARATOR ', ') as nereseni_problemi FROM radninalog left join izvrsenaakcija on izvrsenaakcija.RadniNalog_id=radninalog.id left join akcija on izvrsenaakcija.Akcija_id=akcija.id left join automobil on radninalog.Automobil_id=automobil.id left JOIN model on automobil.Model_id=model.id left join proizvodjac on model.Proizvodjac_id=proizvodjac.id left join klijent on radninalog.Klijent_id=klijent.id left join serviser on izvrsenaakcija.Serviser_id=serviser.id where automobil.id=%s", (id, ))
    automobil = cr.fetchone()
    return flask.jsonify(automobil)


@app.route("/automobil/<int:id>", methods=["GET"])
#@secured(roles=[1,2])
def dobavljanje_jednog_automobila_pri_izmeni(id):
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * from automobil, proizvodjac, model where model.proizvodjac_id=proizvodjac.id and automobil.model_id=model.id and automobil.id=%s", (id, ))
    automobil = cr.fetchone()
    return flask.jsonify(automobil)


@app.route("/automobili/<int:id>", methods=["PUT"])
#@secured(roles=[1])
def izmena_automobila(id):
    db = mysql_db.get_db()
    cr = db.cursor()
    data = dict(request.json)
    data["id"] = id
    cr.execute("UPDATE automobil SET model_id=%(model_id)s, broj_sasije=%(broj_sasije)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200

# -------------------------------------------------------------



#---------------------------AKCIJE-------------------------

@app.route("/akcije", methods=["GET"])
def dobavljanje_akcija():
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM akcija order by naziv_akcije")
    akcije = cr.fetchall()
    return flask.json.jsonify(akcije)

@app.route("/akcije/<int:id>", methods=["GET"])
def dobavljanje_jedne_akcije(id):
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM akcija WHERE id=%s", (id, ))
    akcija = cr.fetchone()
    return flask.json.jsonify(akcija)

@app.route("/akcije", methods=["POST"])
#@secured(roles=[1,2])
def dodavanje_akcije():
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("INSERT INTO akcija (naziv_akcije) VALUES(%(naziv_akcije)s)", request.json)
    db.commit()
    return "", 201

@app.route("/akcije/<int:id>", methods=["PUT"])
#@secured(roles=[1])
def izmena_akcije(id):
    db = mysql_db.get_db()
    cr = db.cursor()
    data = dict(request.json)
    data["id"] = id
    cr.execute("UPDATE akcija SET naziv_akcije=%(naziv_akcije)s, cena_akcije=%(cena_akcije)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200


# -------------------------------------------------------------







#---------------------------SERVISERI/KORISNIK-------------------------


@app.route("/serviseri", methods=["GET"])
@secured(roles=[1])
def dobavljanje_servisera():
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM serviser")
    serviseri = cr.fetchall()
    return flask.json.jsonify(serviseri)


@app.route("/serviseri/<int:id>", methods=["GET"])
@secured(roles=[1])
def dobavljanje_jednog_servisera(id):
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM serviser WHERE id=%s", (id, ))
    serviser = cr.fetchone()
    return flask.json.jsonify(serviser)

@app.route("/serviseri", methods=["POST"])
@secured(roles=[1])
def dodavanje_servisera():
    db = mysql_db.get_db()
    cr = db.cursor()
    data = dict(request.json)
    data["lozinka"] = generate_password_hash(data["lozinka"])
    cr.execute("INSERT INTO serviser (ime_servisera, prezime_servisera, telefon_servisera, korisnicko_ime, lozinka, pravo, obrisan) VALUES(%(ime_servisera)s, %(prezime_servisera)s, %(telefon_servisera)s, %(korisnicko_ime)s, %(lozinka)s, %(pravo)s, %(obrisan)s)", data)
    db.commit()
    return "", 201

@app.route("/serviseri/<int:id>", methods=["PUT"])
@secured(roles=[1])
def izmena_servisera(id):
    db = mysql_db.get_db()
    cr = db.cursor()
    data = dict(request.json)
    data["id"] = id
    cr.execute("UPDATE serviser SET ime_servisera=%(ime_servisera)s, prezime_servisera=%(prezime_servisera)s, telefon_servisera=%(telefon_servisera)s,  korisnicko_ime=%(korisnicko_ime)s, lozinka=%(lozinka)s, pravo=%(pravo)s, obrisan=%(obrisan)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200

@app.route("/brisanjeServisera/<int:id>", methods=["PUT"])
@secured(roles=[1])
def brisanje_servisera(id):
    db = mysql_db.get_db()
    cr = db.cursor()
    data = dict(request.json)
    data["obrisan"] = 1
    cr.execute("UPDATE serviser SET obrisan=%(obrisan)s WHERE id=%(id)s", data)
    db.commit()
    return "", 204

@app.route("/ponistiBrisanjeServisera/<int:id>", methods=["PUT"])
@secured(roles=[1])
def ponistavanje_brisanja_servisera(id):
    db = mysql_db.get_db()
    cr = db.cursor()
    data = dict(request.json)
    data["obrisan"] = 0
    cr.execute("UPDATE serviser SET obrisan=%(obrisan)s WHERE id=%(id)s", data)
    db.commit()
    return "", 204



@app.route("/korisnik", methods=["GET"])
#@secured(roles=[1,2])
def dobavljanje_ulogovanog_korisnika():
    if flask.session.get("korisnik") is not None:
        ulogovani_korisnik = flask.session["korisnik"]
        return flask.jsonify(ulogovani_korisnik)
    else:
        return "", 401

# -------------------------------------------------------------



#---------------------------PRETRAGA-------------------------

@app.route("/radniNalozi/pretraga", methods=["GET"])
@secured(roles=[1,2])
def pretraga_naloga():
    cr = mysql_db.get_db().cursor()
    parametri_pretrage = []
    parametri_pretrage_string = request.args.get("parametri_pretrage_string")
    parametri_pretrage_string = "%" + parametri_pretrage_string + "%"

    parametri_pretrage.append(parametri_pretrage_string)
    parametri_pretrage.append(request.args.get("datumOd"))
    parametri_pretrage.append(request.args.get("datumDo"))
    cr.execute("SELECT * FROM radninalog, klijent, automobil, model, proizvodjac WHERE radninalog.Automobil_id=automobil.id and radninalog.Klijent_id=klijent.id and automobil.Model_id=model.id and model.Proizvodjac_id=proizvodjac.id and CONCAT(proizvodjac.naziv_proizvodjaca, ' ', model.naziv_modela, ' ', ime, ' ', prezime, ' ',  radninalog.problem, ' ', status) LIKE %s and datum_prijema >= %s and DATE(datum_prijema) <= %s order by datum_prijema DESC", parametri_pretrage)
    rezultati_pretrage = cr.fetchall()
    for a in rezultati_pretrage:
        if(a["datum_otpusta"]):
            a["datum_prijema"] = a["datum_prijema"].isoformat()
            a["datum_otpusta"] = a["datum_otpusta"].isoformat()
        else:
            a["datum_prijema"] = a["datum_prijema"].isoformat()
    return flask.jsonify(rezultati_pretrage)




'''@app.route("/radniNalozi/pretragaDatum", methods=["GET"])
#@secured(roles=[1,2])
def pretraga_naloga_datum():
    cr = mysql_db.get_db().cursor()
    datumi = [str(request.args.get("datumOd")), str(request.args.get("datumDo"))]
    cr.execute("SELECT * FROM radninalog, klijent, automobil, model, proizvodjac WHERE radninalog.Automobil_id=automobil.id and radninalog.Klijent_id=klijent.id and automobil.Model_id=model.id and model.Proizvodjac_id=proizvodjac.id and datum_prijema >= %s and datum_prijema <= %s order by datum_prijema DESC", datumi)
    rezultati_pretrage = cr.fetchall()
    for a in rezultati_pretrage:
        if(a["datum_otpusta"]):
            a["datum_prijema"] = a["datum_prijema"].isoformat()
            a["datum_otpusta"] = a["datum_otpusta"].isoformat()
        else:
            a["datum_prijema"] = a["datum_prijema"].isoformat()
    return flask.jsonify(rezultati_pretrage)
'''


# DODATI PRETRAGU PO AKCIJI I NERESENOM PROBLEMU??
@app.route("/automobili/<path:parametri_pretrage>", methods=["GET"])
#@secured(roles=[1,2])
def pretraga_automobila(parametri_pretrage):
    cr = mysql_db.get_db().cursor()
    parametri_pretrage = parametri_pretrage.replace("/", "")
    cr.execute("SELECT *, GROUP_CONCAT(DISTINCT akcija.naziv_akcije SEPARATOR ', ') as sve_akcije, GROUP_CONCAT(IF(radninalog.resen_problem=0, radninalog.problem, NULL) SEPARATOR ', ') as nereseni_problemi FROM radninalog left join izvrsenaakcija on izvrsenaakcija.RadniNalog_id=radninalog.id left join akcija on izvrsenaakcija.Akcija_id=akcija.id left join automobil on radninalog.Automobil_id=automobil.id left JOIN model on automobil.Model_id=model.id left join proizvodjac on model.Proizvodjac_id=proizvodjac.id left join klijent on radninalog.Klijent_id=klijent.id WHERE CONCAT(proizvodjac.naziv_proizvodjaca, ' ', model.naziv_modela, ' ', broj_sasije, ' ', klijent.ime, ' ', klijent.prezime) LIKE %s GROUP BY automobil.id ORDER BY proizvodjac.naziv_proizvodjaca ASC", ("%" + parametri_pretrage + "%"))
    rezultati_pretrage = cr.fetchall()
    return flask.jsonify(rezultati_pretrage)

@app.route("/klijenti/<path:parametri_pretrage>", methods=["GET"])
#@secured(roles=[1,2])
def pretraga_klijenta(parametri_pretrage):
    cr = mysql_db.get_db().cursor()
    parametri_pretrage = parametri_pretrage.replace("/", "")
    cr.execute("SELECT * FROM klijent WHERE CONCAT(ime, ' ', prezime, ' ', telefon) LIKE %s", ("%" + parametri_pretrage + "%"))
    rezultati_pretrage = cr.fetchall()
    return flask.jsonify(rezultati_pretrage)

@app.route("/akcije/<path:parametri_pretrage>", methods=["GET"])
#@secured(roles=[1,2])
def pretraga_akcije(parametri_pretrage):
    cr = mysql_db.get_db().cursor()
    parametri_pretrage = parametri_pretrage.replace("/", "")
    cr.execute("SELECT * FROM akcija WHERE CONCAT(naziv_akcije, ' ', cena_akcije) LIKE %s order by naziv_akcije ASC", ("%" + parametri_pretrage + "%"))
    rezultati_pretrage = cr.fetchall()
    return flask.jsonify(rezultati_pretrage)

#----------------------------------------------------------







if __name__ == "__main__":
    app.run("0.0.0.0", 5000, threaded=True)
