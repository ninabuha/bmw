(function(angular){

    var app = angular.module("app", ["ui.router", "ui.bootstrap"]);

    app.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {

        $stateProvider.state("login", {
            url: "/",
            templateUrl: "/app/components/login/login.html",
            controller: "LoginCtrl",
            controllerAs: "lc"
        }).state({
            name: "radniNalozi",
            url: "/radniNalozi",
            templateUrl: "/app/components/radni_nalozi/radni_nalozi.tpl.html",
            controller: "RadniNaloziCtrl",
            controllerAs: "rnc"
        }).state({
            name: "klijenti",
            url: "/klijenti",
            templateUrl: "/app/components/klijenti/klijenti.tpl.html",
            controller: "KlijentiCtrl",
            controllerAs: "kc"
        }).state({
            name: "automobili",
            url: "/automobili",
            templateUrl: "/app/components/automobili/automobili.tpl.html",
            controller: "AutomobiliCtrl",
            controllerAs: "ac"
        }).state({
            name: "akcije",
            url: "/akcije",
            templateUrl: "/app/components/akcije/akcije.tpl.html",
            controller: "AkcijeCtrl",
            controllerAs: "akc"
        }).state({
            name: "serviseri",
            url: "/serviseri",
            templateUrl: "/app/components/serviseri/serviseri.tpl.html",
            controller: "ServiseriCtrl",
            controllerAs: "sc"
        }).state({
            name: "statistika",
            url: "/statistika",
            templateUrl: "/app/components/statistika/statistika.tpl.html",
            controller: "StatistikaCtrl",
            controllerAs: "stc"
        });
        $urlRouterProvider.otherwise("/radniNalozi");
    }]);



// FACTORY

    app.factory("RestService", ['$http', function($http) {
         var RestService = {};
         var urlBase = "/";
       
         RestService.ulogovaniKorisnikFactory = function(entity) {
           return $http.get(urlBase + entity);
         };
       
         RestService.pretragaFactory = function(entity, data) {
           return $http.get(urlBase + entity + data);
         };
       
         return RestService;
   }]);

})(angular);