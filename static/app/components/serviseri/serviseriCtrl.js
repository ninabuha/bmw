(function (angular) {
    var app = angular.module("app");
    
    app.controller("ServiseriCtrl", ["$http",  "$state", "RestService", function ($http, $state, RestService) {
        var that = this;


        this.ulogovaniKorisnik = {};


        this.serviseri = [];
        this.serviser = {};

        this.parametri_pretrage = "";

        this.noviServiser = {
            "obrisan" : 0
        };


        this.alerts = [];
        this.closeAlert = function(index) {
            that.alerts.splice(index, 1);
        };


        this.dobaviServisere = function(){
        	$http.get("/serviseri").then(function(response){
        		that.serviseri = response.data;
                for(var serviser of that.serviseri){
                    if(serviser.obrisan == 0){
                        serviser.aktivan = "DA";
                    }
                    else if(serviser.obrisan == 1){
                        serviser.aktivan = "NE";
                    }
                }
        	}, function(response){
        		console.log("Greška pri dobavljanju servisera! Kod: " + response.status);
        	})
        }

        this.dobaviServisera = function(id){
            $http.get("/serviseri/" + id).then(function(response){
                that.serviser = response.data;

                // provera da li je serviser aktivan
                if(that.serviser.obrisan == 0){
                    that.serviser.aktivan = "da";
                }
                else if(that.serviser.obrisan == 1){
                    that.serviser.aktivan = "ne";
                }

            }, function(response){
                console.log("Greška pri dobavljanju servisera! Kod: " + response.status);
            })
        }

        this.dodajServisera = function() {
            if(that.noviServiser.lozinka == that.noviServiser.lozinka1){


                $http.post("/serviseri", that.noviServiser).then(function(response){
                    that.noviServiser = {
                        "korisnicko_ime" : "",
                        "lozinka" : "",
                        "pravo" : null,
                        "lozinka1" : ""
                    };

                    that.dobaviServisere();
                    that.alerts.push({ type: 'success', msg: 'Serviser uspešno dodat.' });
                }, function(response){
                    console.log("Greška pri dodavanju servisera! Kod: " + response.status);
                });
            }
            else{
                    that.alerts = [];
                    that.alerts.push({ type: 'danger', msg: 'Serviser neuspešno dodat.' });
            }
        }

        this.izmeniServisera = function(id) {
            if(that.serviser.lozinka == that.serviser.lozinka1){

                $http.put("/serviseri/" + id, that.serviser).then(
                    function(response) {
                        that.dobaviServisera(id);
                        that.dobaviServisere();
                        that.alerts.push({ type: 'success', msg: 'Serviser uspešno izmenjen.' });
                    }, function(response) {
                        console.log("Greška pri izmeni klijenta");
                        that.dobaviServisera(id);
                        that.alerts.push({ type: 'danger', msg: 'Serviser neuspešno izmenjen.' });
                    }
                )
            }
            else{
                that.dobaviServisera(id);
            }
        }

        this.obrisiServisera = function(id){
            $http.put("/brisanjeServisera/" + id, that.serviser).then(
                function(response){
                    that.alerts = [];
                    that.alerts.push({ type: 'success', msg: 'Serviser uspešno obrisan.' });
                    that.dobaviServisera(id);
                    that.dobaviServisere();
                    alert("Ovaj serviser je uspešno obrisan.");
            }, function(response){
                    console.log("Greška pri brisanju servisera! Kod: " + response.status);
                    that.alerts = [];
                    that.alerts.push({ type: 'danger', msg: 'Serviser neuspešno obrisan.' });
            });
        }

        this.ponistiBrisanjeServisera = function(id){
            $http.put("/ponistiBrisanjeServisera/" + id, that.serviser).then(
                function(response){
                    that.alerts = [];
                    that.alerts.push({ type: 'success', msg: 'Servier je uspešno vraćen.' });
                    that.dobaviServisera(id);
                    that.dobaviServisere();
                    alert("Ovaj serviser je uspešno vraćanje.");
            }, function(response){
                console.log("Greška pri vracanju servisera! Kod: " + response.status);
                    that.alerts = [];
                    that.alerts.push({ type: 'danger', msg: 'Neuspešno vraćanje servisera.' });
            });
        }

        this.ocistiFormu = function(){
            document.getElementById("formaDodaj").reset();
        }



        this.ulogovaniKorisnik = function(){
            RestService.ulogovaniKorisnikFactory("korisnik").then(function(response){
                that.ulogovaniKorisnik = response.data;
            }, function(response){
                $state.go("login");
                console.log("Greška pri dobavljanju korisnika! Kod: " + response.status);
            })
        }

        this.ulogovaniKorisnik();
        this.dobaviServisere();

    }]);
})(angular);