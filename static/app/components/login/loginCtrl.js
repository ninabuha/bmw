(function (angular) {
    var app = angular.module("app");

    app.controller("LoginCtrl", ["$http", "$state", function ($http, $state) {

        var that = this;



        this.ulogovaniKorisnik = {korisnicko_ime: "", lozinka: ""};
        this.state = {loggedIn: false};



        

        this.login = function() {
            $http.post("/login", that.ulogovaniKorisnik).then(function(){
                that.state.loggedIn = true;
                $state.go("radniNalozi", {}, {reload: true});
            }, function() {
                that.ulogovaniKorisnik = {korisnicko_ime: "", lozinka: ""}
            })
        }

        this.logout = function() {
            $http.get("/logout").then(function(){
                that.state.loggedIn = false;
                $state.go("login", {}, {reload: true});
            }, function() {

            })
        }

        this.changeBodyBg = function(){
            document.body.style.backgroundColor = "black";
        }


        this.changeBodyBg();

    }]);
})(angular);