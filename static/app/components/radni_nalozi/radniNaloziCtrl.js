(function (angular) {
    var app = angular.module("app");
    
    app.controller("RadniNaloziCtrl", ["$http", "$state", "RestService", function ($http, $state, RestService) {
        var that = this;

        this.otvorenModal = "nalog";

        this.radni_nalozi = [];
        this.radni_nalog = {};
        this.prizvodjac_model = "";
        this.klijent_ime_prezime = "";

        this.proizvodjaci = [];
        this.modeli = [];
        this.automobili = [];
        this.klijenti = [];

        this.izvrsene_akcije = [];
        this.izvrsene_akcije_za_radni_nalog = [];
        var cena_akcija = 0;
        this.akcije = [];
        this.akcija = {};

        this.serviseri = [];

        this.zatvoren = false;


        this.broj_rezultata_pretrage = 0;
        this.ukupan_iznos = 0;



        this.ulogovaniKorisnik = {};





        this.noviRadniNalog = {
            "resen_problem" : 0,
            "status" : "otvoren"
        };

        this.noviAutomobil = {
            "proizvodjac_id" : 0,
            "model_id" : 0,
            "broj_sasije" : ""
        };

        this.noviKlijent = {};

        this.novaIzvrsenaAkcija = {"opis_akcije" : null, "uspesna" : 1}

        this.parametri_pretrage = {
            parametri_pretrage_string : "",
            datumOd : "",
            datumDo : ""
        };





        this.state = {loggedIn: false};


        this.alerts = [];
        this.closeAlert = function(index) {
            that.alerts.splice(index, 1);
        };







        this.logout = function() {
            $http.get("/logout").then(function(){
                that.state.loggedIn = false;
                $state.go("login", {}, {reload: true});
            }, function() {

            })
        }







        this.dobaviRadneNaloge = function() {
            that.ukupan_iznos = 0;
            $http.get("/radniNalozi").then(function(response){
                that.radni_nalozi = response.data;
                that.broj_rezultata_pretrage = that.radni_nalozi.length;
                that.racunanje_ukupnog_iznosa();
            }, function(response) {
                console.log("Greška pri dobavljanju radnih naloga! Kod: " + response.status);
            })
        }

        this.dodajRadniNalog= function() {
            that.noviRadniNalog.automobil_id = that.noviRadniNalog.automobil.id;
            that.noviRadniNalog.klijent_id = that.noviRadniNalog.klijent.id;
            if(that.noviRadniNalog.automobil_id != "" || that.noviRadniNalog.klijent_id != ""){
                that.noviRadniNalog.status = "otvoren";
                $http.post("/radniNalozi", that.noviRadniNalog).then(function(response){
                    that.alerts = [];
                    that.dobaviRadneNaloge();
                    that.noviRadniNalog = {
                        "automobil_id" : 0,
                        "klijent_id" : 0,
                        "problem" : "",
                        "resen_problem" : 0
                    };

                    that.alerts.push({ type: 'success', msg: 'Nalog uspešno dodat.' });
                }, function(response){
                    console.log("Greška pri dodavanju radnog naloga! Kod: " + response.status);
                    that.alerts.push({ type: 'danger', msg: 'Nalog neuspešno dodat.' });
                });
            }
            else{
                that.alerts.push({ type: 'danger', msg: 'Nalog neuspešno dodat.' });
            }
        }

        this.dobaviRadniNalog = function(id) {
            that.generisanaUkupnaCena = 0;
            that.generisanaUkCena = "ne";
            that.dobaviIzvrseneAkcijeZaRadniNalog(id);
            $http.get("/radniNalozi/" + id).then(function(response){
                that.radni_nalog = response.data;
                if(that.radni_nalog.resen_problem == 1){
                    document.getElementById("resen_problem").checked = true;
                }
                that.prizvodjac_model = that.radni_nalog.naziv_proizvodjaca + " " + that.radni_nalog.naziv_modela;
                that.klijent_ime_prezime = that.radni_nalog.ime + " " + that.radni_nalog.prezime;
                if(that.radni_nalog.ukupna_cena == null){
                    that.radni_nalog.ukupna_cena = that.radni_nalog.cena_materijala+cena_akcija;
                }
                if(that.radni_nalog.nabavna_cena_materijala == null){
                    that.radni_nalog.nabavna_cena_materijala = 0;
                }
                if(that.radni_nalog.cena_materijala == null){
                    that.radni_nalog.cena_materijala = 0;
                }
                if(!that.radni_nalog.datum_otpusta){
                    that.radni_nalog.ukupna_cena = that.radni_nalog.cena_materijala+cena_akcija;
                }
                
            }, function(response) {
                console.log("Greška pri dobavljanju radnih naloga! Kod: " + response.status);
            })
        }

        this.generisanaUkupnaCena = 0;
        this.generisanaUkCena = "ne";

        this.generisanjeUkupneCene = function(){
            that.generisanaUkCena = "da";
            that.generisanaUkupnaCena = that.radni_nalog.cena_materijala+cena_akcija;
        }


        this.izmeniRadniNalog = function(id){
            if (document.getElementById('resen_problem').checked) {
                that.radni_nalog.resen_problem = 1;
            } else {
                that.radni_nalog.resen_problem = 0;
            }
            $http.put("/radniNalozi/" + id, that.radni_nalog).then(
                function(response) {
                    that.dobaviRadniNalog(id);
                    that.pretraga();
                    that.alerts.push({ type: 'success', msg: 'Nalog uspešno izmenjen.' });
                }, function(response) {
                    console.log("Greška pri izmeni");
                    that.dobaviRadniNalog(id);
                    that.dobaviRadneNaloge();
                    that.alerts.push({ type: 'danger', msg: 'Nalog neuspešno izmenjen.' });
                }
            )
        }






        this.dobaviProizvodjace = function() {
            $http.get("/proizvodjaci").then(function(response){
                that.proizvodjaci = response.data;
            }, function(response) {
                console.log("Greška pri dobavljanju proizvodjaca! Kod: " + response.status);
            })
        }

        this.dobaviModele = function(id) {
            $http.get("/modeli/" + id).then(function(response){
                that.modeli = response.data;
            }, function(response) {
                console.log("Greška pri dobavljanju modela! Kod: " + response.status);
            })
        }

        this.dobaviAutomobile = function() {
            $http.get("/automobili_radninalozi").then(function(response){
                that.automobili = response.data;
                for (automobil of that.automobili){
                    automobil.proizvodjacModelSasija = automobil.naziv_proizvodjaca + " " + automobil.naziv_modela + " " + automobil.broj_sasije;
                }
            }, function(response) {
                console.log("Greška pri dobavljanju automobila! Kod: " + response.status);
                that.alerts.push({ type: 'danger', msg: 'Vozilo neuspešno dodato.' });
            })
        }

        this.dodajAutomobil = function() {
            that.alerts = [];
            that.noviAutomobil.broj_sasije = that.noviAutomobil.broj_sasije.toUpperCase();
            $http.post("/automobili", that.noviAutomobil).then(function(response){    
                that.noviAutomobil = {
                    "proizvodjac_id" : 0,
                    "model_id" : 0,
                    "broj_sasije" : ""
                }

                that.alerts.push({ type: 'success', msg: 'Vozilo uspešno dodato.' });
            }, function(response){
                console.log("Greška pri dodavanju automobila! Kod: " + response.status);
                that.alerts.push({ type: 'danger', msg: 'Vozilo neuspešno dodato.' });
            });
        }




        this.dobaviKlijente = function() {
            $http.get("/klijenti").then(function(response){
                that.klijenti = response.data;
                for (klijent of that.klijenti){
                    klijent.imePrezimeTelefon = klijent.ime + " " + klijent.prezime + " " + klijent.telefon;
                }
            }, function(response) {
                console.log("Greška pri dobavljanju klijenata! Kod: " + response.status);
            })
        }

        this.dodajKlijenta = function() {
            $http.post("/klijenti", that.noviKlijent).then(function(response){
                that.alerts = [];
                that.noviKlijent = {
                    "ime" : "",
                    "prezime" : "",
                    "telefon" : ""
                };
                that.dobaviKlijente();
                that.alerts.push({ type: 'success', msg: 'Klijent uspešno dodat.' });
            }, function(response){
                console.log("Greška pri dodavanju klijenta! Kod: " + response.status);
                that.alerts.push({ type: 'danger', msg: 'Klijent neuspešno dodat.' });
            });
        }




        

        this.zatvaranjeNaloga = function(id){
            that.generisanaUkCena = "ne";
            that.generisanjeUkupneCene();
            that.radni_nalog.ukupna_cena = that.generisanaUkupnaCena;
            $http.put("/radniNalozi/zatvaranje/" + id, that.radni_nalog).then(
                function(response) {
                    that.dobaviRadniNalog(id);
                    that.pretraga();
                    that.alerts.push({ type: 'success', msg: 'Nalog uspešno zatvoren.' });
                    alert("Ovaj nalog je uspešno zatvoren.");
                }, function(response) {
                    console.log("Greška pri izmeni");
                    that.dobaviRadniNalog(id);
                    that.pretraga();
                    that.alerts.push({ type: 'danger', msg: 'Nalog neuspešno zatvoren.' });
                }
            )
        }

        this.otvaranjeNaloga = function(id){
            if(that.radni_nalog.ukupna_cena == null){
            that.radni_nalog.ukupna_cena = 0;
            }
            if(that.radni_nalog.nabavna_cena_materijala == null){
                that.radni_nalog.nabavna_cena_materijala = 0;
            }
            if(that.radni_nalog.cena_materijala == null){
                that.radni_nalog.cena_materijala = 0;
            }
            that.radni_nalog.ukupna_cena = that.radni_nalog.cena_materijala+cena_akcija;

            $http.put("/radniNalozi/otvaranje/" + id, that.radni_nalog).then(
                function(response) {
                    that.dobaviRadniNalog(id);
                    that.pretraga();
                    that.alerts.push({ type: 'success', msg: 'Nalog uspešno izmenjen.' });
                    alert("Ovaj nalog je otvoren.");
                    if(that.izvrsene_akcije_za_radni_nalog != 0){
                        that.statusAktivan(id);
                    }
                }, function(response) {
                    that.dobaviRadniNalog(id);
                    that.pretraga();
                    that.alerts.push({ type: 'danger', msg: 'Nalog neuspešno izmenjen.' });
                }
            )
        }


        this.dobaviIzvrseneAkcije = function(){
            $http.get("/izvrseneAkcije").then(function(response){
                that.izvrsene_akcije = response.data;
            }, function(response) {
                console.log("Greška pri dobavljanju izvrsenih akcija! Kod: " + response.status);
            })
        }


        this.dobaviIzvrseneAkcijeZaRadniNalog = function(id){
            $http.get("/izvrseneAkcije/" + id).then(function(response){
                that.izvrsene_akcije_za_radni_nalog = response.data;
                cena_akcija = 0;
                for(akcija of that.izvrsene_akcije_za_radni_nalog){
                    cena_akcija += akcija.cena_izvrsene_akcije;
                }

            }, function(response) {
                console.log("Greška pri dobavljanju izvrsenih akcija! Kod: " + response.status);
            })
        }

        this.dobaviAkcije = function(){
            $http.get("/akcije").then(function(response){
                that.akcije = response.data;
                for(akcija of that.akcije){
                    akcija.nazivCena = akcija.naziv_akcije + " " + akcija.cena_akcije;
                }
            }, function(response){
                console.log("Greška pri dobavljanju akcija! Kod: " + response.status);
            })
        }

        this.dobaviAkciju = function(id){
            $http.get("/akcije/" + id).then(function(response){
                that.akcija = response.data;
            }, function(response){
                console.log("Greška pri dobavljanju akcije! Kod: " + response.status);
            })
        }

        this.obrisiAkciju = function(id) {
            $http.delete("/akcija/" + id, that.akcija).then(
                function(response){
                    that.alerts = [];
                    that.alerts.push({ type: 'success', msg: 'Akcija uspešno obrisana.' });
                    that.dobaviIzvrseneAkcijeZaRadniNalog(that.radni_nalog.id);
                    that.dobaviRadniNalog(that.radni_nalog.id);
            }, function(response){
                    console.log("Greška pri uklanjanju akcije! Kod: " + response.status);
                    that.alerts = [];
                    that.alerts.push({ type: 'danger', msg: 'Akcija neuspešno obrisana.' });
            });
        }

        this.dodajIzvrsenuAkciju = function(){
            that.novaIzvrsenaAkcija.akcija_id = that.novaIzvrsenaAkcija.akcija.id;
            if (document.getElementById('uspesna').checked) {
                that.novaIzvrsenaAkcija.uspesna = 1;
            } else {
                that.novaIzvrsenaAkcija.uspesna = 0;
            }
            if(that.zatvoren == true){

            }
            that.novaIzvrsenaAkcija.radniNalog_id = that.radni_nalog.id;
            that.novaIzvrsenaAkcija.cena_izvrsene_akcije = that.novaIzvrsenaAkcija.akcija.cena_akcije;
            $http.post("/izvrseneAkcije", that.novaIzvrsenaAkcija).then(function(response) {
                    that.novaIzvrsenaAkcija = {"opis_akcije" : null, "uspesna" : 1}
                    that.dobaviIzvrseneAkcijeZaRadniNalog(that.radni_nalog.id);
                    that.dobaviRadniNalog(that.radni_nalog.id);
                    that.pretraga();
                    that.alerts.push({ type: 'success', msg: 'Akcija uspešno dodata.' });
                    if(that.radni_nalog.status == "otvoren"){
                        that.statusAktivan(that.radni_nalog.id);
                    }
                    
                }, function(response) {
                    console.log("Greška pri dodavanju izvrsene akcije");
                    that.alerts.push({ type: 'danger', msg: 'Akcija neuspešno dodata.' });
                }
            )
        }

        this.dobaviServisere = function(){
            $http.get("/serviseri").then(function(response){
                for(serviser of response.data){
                    if(serviser.obrisan == 0){
                        that.serviseri.push(serviser);
                    }
                }
            }, function(response){
                console.log("Greška pri dobavljanju servisera! Kod: " + response.status);
            })
        }

        this.statusAktivan = function(id){
            $http.put("/radniNalozi/aktivan/" + id, that.radni_nalog).then(
                function(response) {
                    that.dobaviRadniNalog(id);
                    that.pretraga();
                }, function(response) {
                    console.log("Greška pri izmeni statusa");
                    that.dobaviRadniNalog(id);
                    that.pretraga();
                }
            )
        }



        this.razduzivanjeNaloga = function(id){
            $http.put("/radniNalozi/razduzen/" + id, that.radni_nalog).then(
                function(response) {
                    that.dobaviRadniNalog(id);
                    that.pretraga();
                    that.alerts.push({ type: 'success', msg: 'Nalog uspešno razdužen.' });
                }, function(response) {
                    console.log("Greška pri izmeni statusa");
                    that.dobaviRadniNalog(id);
                    that.pretraga();
                    that.alerts.push({ type: 'danger', msg: 'Nalog neuspešno razdužen.' });
                }
            )
        }








        this.ocistiFormu = function(){
            document.getElementById("formaDodaj").reset();
            if(document.getElementById("formaDodajNalog")){
                document.getElementById("formaDodajNalog").reset();
            }
            if(document.getElementById("formaDodajVozilo")){
                document.getElementById("formaDodajVozilo").reset();
            }
            if(document.getElementById("formaDodajKlijnta")){
                document.getElementById("formaDodajKlijnta").reset();
            }
        }





        this.datumOd = "";
        this.datumDo = "";


        this.pretraga = function(){
            that.ukupan_iznos = 0;
            that.parametri_pretrage.datumOd = document.getElementById("datumOd").value;
            that.parametri_pretrage.datumDo = document.getElementById("datumDo").value;

            if(that.parametri_pretrage.datumOd == ""){
                that.parametri_pretrage.datumOd = "1980-02-02"
            }
            if(that.parametri_pretrage.datumDo == ""){
                var today = new Date();
                var dd = String(today.getDate()).padStart(2, '0');
                var mm = String(today.getMonth() + 1).padStart(2, '0');
                var yyyy = today.getFullYear();

                today = yyyy + '-' + mm + '-' + dd;
                that.parametri_pretrage.datumDo = today;
            }
            $http.get("/radniNalozi/pretraga", {params: that.parametri_pretrage}).then(function(response){
                that.radni_nalozi = response.data;
                that.broj_rezultata_pretrage = that.radni_nalozi.length;
                that.ukupan_iznos = 0;
                that.racunanje_ukupnog_iznosa();
            }, function(response){
                console.log("Greška pri pretrazi radnog naloga! Kod: " + response.status);
                that.dobaviRadneNaloge();
            })
        }


        this.racunanje_ukupnog_iznosa = function(){
            for(radni_nalog of that.radni_nalozi){
                that.ukupan_iznos += radni_nalog.ukupna_cena;
            }
        }



        this.ocistiDatume = function(){
            document.getElementById("datumOd").value = "";
            document.getElementById("datumDo").value = "";
            that.pretraga();
        }







// KORISCENJE FACTORY

        this.ulogovaniKorisnik = function(){
            RestService.ulogovaniKorisnikFactory("korisnik").then(function(response){
                that.ulogovaniKorisnik = response.data;
            }, function(response){
                $state.go("login");
                console.log("Greška pri dobavljanju korisnika! Kod: " + response.status);
            })
        }

        this.changeBodyBg = function(){
            document.body.style.backgroundColor = "white";
        }


        this.changeBodyBg();
        this.ulogovaniKorisnik();
        this.dobaviRadneNaloge();
        
    }]);
})(angular);