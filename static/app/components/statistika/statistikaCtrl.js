(function (angular) {
    var app = angular.module("app");
    
    app.controller("StatistikaCtrl", ["$http", "$state", "RestService", function ($http, $state, RestService) {
        var that = this;


        this.radni_nalozi = [];
        this.izvrsene_akcije = [];
        this.serviseri = [];

        this.diagram = "chartRadniNalozi";


        var chartRadniNalozi = new CanvasJS.Chart("chartRadniNalozi");
        var chartAkcije = new CanvasJS.Chart("chartAkcije");
        var chartPrihod = new CanvasJS.Chart("chartPrihod");



        var brojOtvorenihNalogaPoMesecima = [0,0,0,0,0,0,0,0,0,0,0,0];
        var brojZatvorenihNalogaPoMesecima = [0,0,0,0,0,0,0,0,0,0,0,0];

        var prihodPoMesecima = [0,0,0,0,0,0,0,0,0,0,0,0];






        this.ulogovaniKorisnik = {};



        this.setAll = function(lista) {
            for (i = 0; i < lista.length; ++i) {
                lista[i] = 0;
            }
        }




        this.dobaviRadneNaloge = function(){
            that.setAll(brojOtvorenihNalogaPoMesecima);
            that.setAll(brojZatvorenihNalogaPoMesecima);

            $http.get("/radniNalozi").then(function(response){
                for(radni_nalog of response.data){
                    var tekucaGodina = new Date();
                    tekucaGodina = tekucaGodina.getFullYear();
                    var datum = new Date(radni_nalog.datum_prijema);
                    if(tekucaGodina == datum.getFullYear()){
                        var mesecOtvaranja = datum.getMonth();

                        brojOtvorenihNalogaPoMesecima[mesecOtvaranja]++;
                    }


                    var datum_otpusta = new Date(radni_nalog.datum_otpusta);
                    if(datum_otpusta){
                        if(tekucaGodina == datum_otpusta.getFullYear()){
                            var mesecZatvaranja = datum_otpusta.getMonth();

                            brojZatvorenihNalogaPoMesecima[mesecZatvaranja]++;
                        }
                    }
                }

                chartAkcije.destroy();
                chartPrihod.destroy();
                that.prikaziDijagramNaloga();
            }, function(response){
                console.log("Greška pri dobavljanju Statistike! Kod: " + response.status);
            })
        }


        this.prikaziDijagramNaloga = function(){
            that.diagram = "chartRadniNalozi";
            chartRadniNalozi = new CanvasJS.Chart("chartRadniNalozi", {
                animationEnabled: true,  
                title:{
                    text: "Radni nalozi (godišnji prikaz)",
                    fontFamily: 'Quicksand'
                },
                legend:{
                    fontFamily: "Quicksand",
                },
                axisY: {
                    title: "Broj radnih naloga",
                    valueFormatString: "#",
                    labelFontFamily: "Quicksand",
                },
                toolTip:{
                    shared:true
                },
                data: [{
                    type: "line",
                    name: "Otvoreni nalozi",
                    showInLegend: true,
                    markerType: "square",
                    color: "#19AD5E",
                    dataPoints: [
                            {label:"Januar", y:brojOtvorenihNalogaPoMesecima[0]},
                            {label:"Februar", y:brojOtvorenihNalogaPoMesecima[1]},
                            {label:"Mart", y:brojOtvorenihNalogaPoMesecima[2]},
                            {label:"April", y:brojOtvorenihNalogaPoMesecima[3]},
                            {label:"Maj", y:brojOtvorenihNalogaPoMesecima[4]},
                            {label:"Jun", y:brojOtvorenihNalogaPoMesecima[5]},
                            {label:"Jul", y:brojOtvorenihNalogaPoMesecima[6]},
                            {label:"Avgust", y:brojOtvorenihNalogaPoMesecima[7]},
                            {label:"Septembar", y:brojOtvorenihNalogaPoMesecima[8]},
                            {label:"Oktobar", y:brojOtvorenihNalogaPoMesecima[9]},
                            {label:"Novembar", y:brojOtvorenihNalogaPoMesecima[10]},
                            {label:"Decembar", y:brojOtvorenihNalogaPoMesecima[11]}
                    ]
                },
                {
                    type: "line",
                    name: "Zatvoreni nalozi", 
                    fontFamily: 'Quicksand',     
                    showInLegend: true,
                    markerType: "circle",
                    color: "#F08080",
                    lineDashType: "dash",
                    dataPoints: [
                            {label:"Januar", y:brojZatvorenihNalogaPoMesecima[0]},
                            {label:"Februar", y:brojZatvorenihNalogaPoMesecima[1]},
                            {label:"Mart", y:brojZatvorenihNalogaPoMesecima[2]},
                            {label:"April", y:brojZatvorenihNalogaPoMesecima[3]},
                            {label:"Maj", y:brojZatvorenihNalogaPoMesecima[4]},
                            {label:"Jun", y:brojZatvorenihNalogaPoMesecima[5]},
                            {label:"Jul", y:brojZatvorenihNalogaPoMesecima[6]},
                            {label:"Avgust", y:brojZatvorenihNalogaPoMesecima[7]},
                            {label:"Septembar", y:brojZatvorenihNalogaPoMesecima[8]},
                            {label:"Oktobar", y:brojZatvorenihNalogaPoMesecima[9]},
                            {label:"Novembar", y:brojZatvorenihNalogaPoMesecima[10]},
                            {label:"Decembar", y:brojZatvorenihNalogaPoMesecima[11]}
                    ]
                }]
            });
            chartRadniNalozi.render();
        }






        var paroviAkcije = [];
        var paroviZarade = [];

        this.dobaviServisere = function(){
            $http.get("/serviseri").then(function(response){
                that.serviseri = response.data;
                for(serviser of that.serviseri){
                    serviser.brojIzvrsenihAkcija = 0;
                    serviser.ukupnaZaradaNaAkcijama = 0;
                }

                chartRadniNalozi.destroy();
                chartPrihod.destroy();
                that.dobaviIzvrseneAkcije();
            }, function(response){
                console.log("Greška pri dobavljanju servisera! Kod: " + response.status);
            })
        }

        this.dobaviIzvrseneAkcije = function(){
            $http.get("/izvrseneAkcije").then(function(response){
                that.izvrsene_akcije = response.data;
                var tekucaGodina = new Date();
                tekucaGodina = tekucaGodina.getFullYear();
                
                for(izvrsena_akcija of that.izvrsene_akcije){
                    var datum_izvrsavanja = new Date(izvrsena_akcija.datum_izvrsavanja);
                    if(tekucaGodina == datum_izvrsavanja.getFullYear()){
                        for(serviser of that.serviseri){
                            if(izvrsena_akcija.Serviser_id == serviser.id){
                                serviser.ukupnaZaradaNaAkcijama += izvrsena_akcija.cena_izvrsene_akcije;
                                serviser.brojIzvrsenihAkcija++;
                            }
                        }
                    }
                }

                paroviAkcije.length = 0;
                paroviZarade.length = 0;
                for(serviser of that.serviseri){
                    paroviAkcije.push({"label" : serviser.korisnicko_ime, y: serviser.brojIzvrsenihAkcija});
                    paroviZarade.push({"label" : serviser.korisnicko_ime, y: serviser.ukupnaZaradaNaAkcijama});
                }
                
                that.prikaziDijagramAkcija();
            }, function(response) {
                console.log("Greška pri dobavljanju izvrsenih akcija! Kod: " + response.status);
            })
        }

        this.prikaziDijagramAkcija = function(){
            that.diagram = "chartAkcije";
            chartAkcije = new CanvasJS.Chart("chartAkcije", {
                animationEnabled: true,  
                title:{
                    text: "Izvršene akcije (godišnji prikaz)",
                    fontFamily: 'Quicksand'
                },
                legend:{
                    fontFamily: "Quicksand",
                },
                axisY: {
                    title: "Broj izvršenih akcija",
                    valueFormatString: "#",
                    labelFontFamily: "Quicksand",
                },
                toolTip:{
                    shared:true
                },
                data: [{
                    type: "column",
                    name: "Broj izvršenih akcija",
                    showInLegend: true,
                    markerType: "square",
                    color: "rgba(255,12,32,.5)",
                    dataPoints: paroviAkcije
                },
                {
                    type: "column", 
                    name: "Ukupna zarada na akcijama",
                    axisYType: "secondary",
                    color: "rgba(128,15,15, .8)",
                    showInLegend: true,
                    dataPoints: paroviZarade
                }]
            });
            chartAkcije.render();
        }





        this.dobaviPrihod = function() {
            that.setAll(prihodPoMesecima);
            $http.get("/radniNalozi").then(function(response){
                for(radni_nalog of response.data){
                    if(!radni_nalog.ukupna_cena){
                        radni_nalog.ukupna_cena = 0;
                    }
                    var tekucaGodina = new Date();
                    tekucaGodina = tekucaGodina.getFullYear();
                    var datum = new Date(radni_nalog.datum_otpusta);
                    if(tekucaGodina == datum.getFullYear()){
                        var mesecZatvaranja = datum.getMonth();

                        prihodPoMesecima[mesecZatvaranja]+=(radni_nalog.ukupna_cena-radni_nalog.nabavna_cena_materijala);

                    }
                }
                chartAkcije.destroy();
                chartRadniNalozi.destroy();
                that.prikaziDijagramPrihoda();
            }, function(response) {
                console.log("Greška pri dobavljanju radnih naloga! Kod: " + response.status);
            })
        }


        this.prikaziDijagramPrihoda = function(){
            that.diagram = "chartPrihod";
            chartPrihod = new CanvasJS.Chart("chartPrihod", {
                animationEnabled: true,  
                title:{
                    text: "Prihodi (godišnji prikaz)",
                    fontFamily: 'Quicksand'
                },
                legend:{
                    fontFamily: "Quicksand",
                },
                axisY: {
                    valueFormatString: "#",
                    labelFontFamily: "Quicksand",
                },
                toolTip:{
                    shared:true
                },
                data: [{
                    type: "pie",
                    toolTipContent: "<b>{label}</b>: {y} RSD",
                    name: "Prihodi",
                    showInLegend: true,
                    legendText: "{label}",
                    dataPoints: [
                            {label:"Januar", y:prihodPoMesecima[0]},
                            {label:"Februar", y:prihodPoMesecima[1]},
                            {label:"Mart", y:prihodPoMesecima[2]},
                            {label:"April", y:prihodPoMesecima[3]},
                            {label:"Maj", y:prihodPoMesecima[4]},
                            {label:"Jun", y:prihodPoMesecima[5]},
                            {label:"Jul", y:prihodPoMesecima[6]},
                            {label:"Avgust", y:prihodPoMesecima[7]},
                            {label:"Septembar", y:prihodPoMesecima[8]},
                            {label:"Oktobar", y:prihodPoMesecima[9]},
                            {label:"Novembar", y:prihodPoMesecima[10]},
                            {label:"Decembar", y:prihodPoMesecima[11]}
                    ]
                }]
            });
            chartPrihod.render();
        }










        this.ulogovaniKorisnik = function(){
            RestService.ulogovaniKorisnikFactory("korisnik").then(function(response){
                that.ulogovaniKorisnik = response.data;
            }, function(response){
                $state.go("login");
                console.log("Greška pri dobavljanju korisnika! Kod: " + response.status);
            })
        }

        this.changeBodyBg = function(){
            document.body.style.backgroundColor = "white";
        }


        this.changeBodyBg();
        this.ulogovaniKorisnik();
        this.dobaviRadneNaloge();
        this.prikaziDijagramNaloga();
    }]);
})(angular);