(function (angular) {
    var app = angular.module("app");
    
    app.controller("AutomobiliCtrl", ["$http",  "$state", "RestService", function ($http, $state, RestService) {
        var that = this;

        this.otvorenModal = "proizvodjac";


        this.proizvodjaci = [];
        this.modeli = [];
        this.automobili = [];
        this.automobil = {};

        this.izvrsene_akcije_za_automobil = [];


        this.parametri_pretrage = "";

        this.noviAutomobil = {
            "proizvodjac_id" : 0,
            "model_id" : 0,
            "broj_sasije" : ""
        };

        this.noviProizvodjac = {};
        this.noviModel = {};


        this.ulogovaniKorisnik = {};


        this.alerts = [];
        this.closeAlert = function(index) {
            that.alerts.splice(index, 1);
        };




        this.dobaviProizvodjace = function() {
            $http.get("/proizvodjaci").then(function(response){
                that.proizvodjaci = response.data;
            }, function(response) {
                console.log("Greška pri dobavljanju proizvodjaca! Kod: " + response.status);
            })
        }

        this.dodajProizvodjaca = function(){
            $http.post("/proizvodjaci", that.noviProizvodjac).then(function(response) {
                    that.noviProizvodjac = {};
                    that.dobaviProizvodjace();
                    that.alerts.push({ type: 'success', msg: 'Proizvođač uspešno dodat.' });
                }, function(response) {
                    console.log("Greška pri dodavanju proizvodjaca");
                    that.alerts.push({ type: 'danger', msg: 'Proizvođač neuspešno dodat.' });
                }
            )
        }

        this.dobaviModele = function(id) {
            $http.get("/modeli/" + id).then(function(response){
                that.modeli = response.data;
            }, function(response) {
                console.log("Greška pri dobavljanju modela! Kod: " + response.status);
            })
        }

        this.dodajModel = function(){
            $http.post("/modeli", that.noviModel).then(function(response) {
                    that.noviModel = {};
                    that.dobaviProizvodjace();
                    that.alerts.push({ type: 'success', msg: 'Model uspešno dodat.' });
                }, function(response) {
                    console.log("Greška pri dodavanju modela");
                    that.alerts.push({ type: 'success', msg: 'Model neuspešno dodat.' });
                }
            )
        }


        this.dobaviAutomobile = function() {
            $http.get("/automobili").then(function(response){
                that.automobili = response.data;
            }, function(response) {
                console.log("Greška pri dobavljanju automobila! Kod: " + response.status);
            })
        }

        this.dobaviAutomobil = function(id) {
            $http.get("/automobili/" + id).then(function(response) {
                that.automobil = response.data;
                that.proizvodjac = {id : that.automobil.Proizvodjac_id, naziv : that.automobil.naziv_proizvodjaca};
                that.model = {id : that.automobil.Model_id, naziv : that.automobil.naziv_modela};
            }, function(response) {
                console.log(response.status);
            });
        }

        this.dodajAutomobil = function() {
            that.alerts = [];
            that.noviAutomobil.broj_sasije = that.noviAutomobil.broj_sasije.toUpperCase();
            $http.post("/automobili", that.noviAutomobil).then(function(response){    
                that.noviAutomobil = {
                    "proizvodjac_id" : 0,
                    "model_id" : 0,
                    "broj_sasije" : ""
                }

                that.alerts.push({ type: 'success', msg: 'Vozilo uspešno dodato.' });
            }, function(response){
                console.log("Greška pri dodavanju automobila! Kod: " + response.status);
                that.alerts.push({ type: 'danger', msg: 'Vozilo neuspešno dodato.' });
            });
        }


        this.dobaviIzvrseneAkcijeZaAutomobil = function(id){
            $http.get("/izvrseneAkcije/automobil/" + id).then(function(response){
                that.izvrsene_akcije_za_automobil = response.data;
            }, function(response) {
                console.log("Greška pri dobavljanju izvrsenih akcija! Kod: " + response.status);
            })
        }

        

        this.pretraga = function(){
            if(that.parametri_pretrage!=""){
                RestService.pretragaFactory("automobili/", that.parametri_pretrage + "/").then(function(response){
                    that.automobili = response.data;
                }, function(response){
                    console.log("Greška pri pretrazi automobila! Kod: " + response.status);
                })
            }
            else{
                that.dobaviAutomobile();
            }
        }



        this.ocistiFormu = function(){
            document.getElementById("formaDodaj").reset();
            if(document.getElementById("formaDodajProizvodjaca")){
                document.getElementById("formaDodajProizvodjaca").reset();
            }
            if(document.getElementById("formaDodajModel")){
                document.getElementById("formaDodajModel").reset();
            }
        }



        this.ulogovaniKorisnik = function(){
            $http.get("/korisnik").then(function(response){
                that.ulogovaniKorisnik = response.data;
            }, function(response){
                $state.go("login");
                console.log("Greška pri dobavljanju korisnika! Kod: " + response.status);
            })
        }






        this.ulogovaniKorisnik();
        this.dobaviProizvodjace();
        this.dobaviAutomobile();

    }]);
})(angular);