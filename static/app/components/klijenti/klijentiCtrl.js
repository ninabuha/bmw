(function (angular) {
    var app = angular.module("app");
    
    app.controller("KlijentiCtrl", ["$http", "$state", "RestService", function ($http, $state, RestService) {
        var that = this;

        this.klijenti = [];
        this.klijent = {};

        this.noviKlijent = {};

        this.ulogovaniKorisnik = {};





        this.alerts = [];
        this.closeAlert = function(index) {
            that.alerts.splice(index, 1);
        };








        this.dobaviKlijente = function() {
            $http.get("/klijenti").then(function(response){
                that.klijenti = response.data;
            }, function(response) {
                console.log("Greška pri dobavljanju klijenata! Kod: " + response.status);
            })
        }

        this.dobaviKlijenta = function(id) {
            $http.get("/klijenti/"+id).then(function(response) {
                if(response.data.obrisan == "0"){
                    response.data.obrisan = "ne";
                }
                else if(response.data.obrisan == "1"){
                    response.data.obrisan = "da";
                }
                that.klijent = response.data;
            }, function(response) {
                console.log(response.status);
            });
        }

        this.dodajKlijenta = function() {
            $http.post("/klijenti", that.noviKlijent).then(function(response){
                that.alerts = [];
                that.noviKlijent = {
                    "ime" : "",
                    "prezime" : "",
                    "telefon" : ""
                };
                that.dobaviKlijente();
                that.alerts.push({ type: 'success', msg: 'Klijent uspešno dodat.' });
            }, function(response){
                console.log("Greška pri dodavanju klijenta! Kod: " + response.status);
                that.alerts.push({ type: 'danger', msg: 'Klijent neuspešno dodat.' });
            });
        }

        this.izmeniKlijenta = function(id) {
            $http.put("/klijenti/" + id, that.klijent).then(
                function(response) {
                    that.dobaviKlijenta(that.klijent.id);
                    that.dobaviKlijente();
                    that.alerts.push({ type: 'success', msg: 'Klijent uspešno izmenjen.' });
                }, function(response) {
                    console.log("Greška pri izmeni klijenta");
                    that.dobaviKlijenta(that.klijent.id);
                    that.alerts.push({ type: 'danger', msg: 'Klijent neuspešno izmenjen.' });
                }
            )
        }




        this.ocistiFormu = function(){
            document.getElementById("formaDodaj").reset();
            if(document.getElementById("formaDodajNalog")){
                document.getElementById("formaDodajNalog").reset();
            }
            if(document.getElementById("formaDodajVozilo")){
                document.getElementById("formaDodajVozilo").reset();
            }
            if(document.getElementById("formaDodajKlijnta")){
                document.getElementById("formaDodajKlijnta").reset();
            }
        }






        this.pretraga = function(){
            if(that.parametri_pretrage!=""){
                RestService.pretragaFactory("klijenti/", that.parametri_pretrage + "/").then(function(response){
                    that.klijenti = response.data;
                }, function(response){
                    console.log("Greška pri pretrazi klijenta! Kod: " + response.status);
                })
            }
            else{
                that.dobaviKlijente();
            }
        }








        this.ulogovaniKorisnik = function(){
            $http.get("/korisnik").then(function(response){
                that.ulogovaniKorisnik = response.data;
            }, function(response){
                $state.go("login");
                console.log("Greška pri dobavljanju korisnika! Kod: " + response.status);
            })
        }

        this.changeBodyBg = function(){
            document.body.style.backgroundColor = "white";
        }


        this.changeBodyBg();
        this.ulogovaniKorisnik();
        this.dobaviKlijente();
    }]);
})(angular);