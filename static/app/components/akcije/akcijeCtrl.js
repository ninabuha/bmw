(function (angular) {
    var app = angular.module("app");
    
    app.controller("AkcijeCtrl", ["$http",  "$state", "RestService", function ($http, $state, RestService) {
        var that = this;


        this.admin = {korisnicko_ime: "", lozinka: ""}
        this.ulogovaniKorisnik = {};


        this.akcije = [];
        this.akcija = {};

        this.parametri_pretrage = "";

        this.novaAkcija = {
            
        };


        this.ulogovaniKorisnik = {};


        this.alerts = [];
        this.closeAlert = function(index) {
            that.alerts.splice(index, 1);
        };



        this.dobaviAkcije = function(){
        	$http.get("/akcije").then(function(response){
        		that.akcije = response.data;
        	}, function(response){
        		console.log("Greška pri dobavljanju akcija! Kod: " + response.status);
        	})
        }

        this.dobaviAkciju = function(id) {
            $http.get("/akcije/"+id).then(function(response) {
                that.akcija = response.data;
            }, function(response) {
                console.log(response.status);
            });
        }

        this.dodajAkciju = function() {
            $http.post("/akcije", that.novaAkcija).then(function(response){
                that.novaAkcija = {
                    "naziv_akcije" : "",
                    "cena_akcije" : ""
                };
                that.dobaviAkcije();
                that.alerts.push({ type: 'success', msg: 'Akcija uspešno dodata.' });
            }, function(response){
                console.log("Greška pri dodavanju akcije! Kod: " + response.status);
                that.alerts.push({ type: 'danger', msg: 'Akcija neuspešno dodata.' });
            });
        }

        this.izmeniAkciju = function(id) {
            $http.put("/akcije/" + id, that.akcija).then(
                function(response) {
                    that.dobaviAkciju(id);
                    that.dobaviAkcije();
                    that.alerts.push({ type: 'success', msg: 'Akcija uspešno izmenjena.' });
                }, function(response) {
                    console.log("Greška pri izmeni akcije");
                    that.alerts.push({ type: 'danger', msg: 'Akcija neuspešno izmenjena.' });
                }
            )
        }


        this.ocistiFormu = function(){
        	document.getElementById("formaDodaj").reset();
        }




        this.pretraga = function(){
            if(that.parametri_pretrage!=""){
                RestService.pretragaFactory("akcije/", that.parametri_pretrage + "/").then(function(response){
                    that.akcije = response.data;
                }, function(response){
                    console.log("Greška pri pretrazi akcije! Kod: " + response.status);
                })
            }
            else{
                that.dobaviAkcije();
            }
        }



        this.ulogovaniKorisnik = function(){
            $http.get("/korisnik").then(function(response){
                that.ulogovaniKorisnik = response.data;
            }, function(response){
                $state.go("login");
                console.log("Greška pri dobavljanju korisnika! Kod: " + response.status);
            })
        }





        this.dobaviAkcije();
        this.ulogovaniKorisnik();

    }]);
})(angular);