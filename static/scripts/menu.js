function openNav() {
	if(document.getElementById("addButton1")){
		document.getElementById("addButton1").style.display = "none";
	}
	if(document.getElementById("addButton")){
		document.getElementById("addButton").style.display = "none";
	}
	document.getElementById("user").style.display = "inline-block";
	document.getElementById("userInput").style.display = "inline-block";
  	document.getElementById("mySidebar").style.width = "250px";
}

function closeNav() {
	if(document.getElementById("addButton1")){
		document.getElementById("addButton1").style.display = "inline-block";
	}
	if(document.getElementById("addButton")){
		document.getElementById("addButton").style.display = "inline-block";
	}
	document.getElementById("user").style.display = "none";
	document.getElementById("userInput").style.display = "none";
  	document.getElementById("mySidebar").style.width = "0";
  	document.getElementById("main").style.marginLeft= "0";
}